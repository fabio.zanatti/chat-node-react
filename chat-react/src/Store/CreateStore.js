import { createStore } from 'redux';
// import { routerReducer, routerMiddleware } from 'react-router-redux';
// import thunk from 'redux-thunk';
// import history from '../Utils/BrowserHistory';
// import reducers from '../Containers/Reducers';
// import clientMiddleware from '../Utils/ClientMiddleware';

// const routeMiddleware = routerMiddleware(history);
// const middlewares = [thunk, routeMiddleware, clientMiddleware];

// const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(); //eslint-disable-line

const store = createStore();

export { store };
