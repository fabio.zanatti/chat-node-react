import React from "react";
import { Switch, Route } from "react-router-dom";
import ContentLayout from "../Containers/ContentLayout";
import asyncComponent from "../Utils/AsyncFunc";

const ContentLayoutRoute = ({
  component: Component,
  titulo,
  icon,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => (
      <ContentLayout {...props} titulo={titulo} icon={icon}>
        <Component {...props} />
      </ContentLayout>
    )}
  />
);

const Acompanhamento = asyncComponent(() => import("../Pages/Acompanhamento"));
const Exercicio = asyncComponent(() => import("../Pages/Cadastros/Exercicios"));
const GrupoMuscular = asyncComponent(() => import("../Pages/Cadastros/GrupoMuscular"));
const Treinos = asyncComponent(() => import("../Pages/Cadastros/Treino"));
const NotFound = asyncComponent(() => import("./Erro/404"));

const PrivateRoutes = ({ url }) => {
  console.log("url", url + "/cadastro-exercicio");
  return (
    <Switch>
      <Route exact path={`${url}`} component={Acompanhamento} />
      <ContentLayoutRoute
        path={`${url}/cadastro-exercicios`}
        icon="file"
        titulo="Cadastro de Exercício"
        component={Exercicio}
      />
      <ContentLayoutRoute
        path={`${url}/cadastro-grupo-muscular`}
        icon="file"
        titulo="Cadastro de Grupo Muscular"
        component={GrupoMuscular}
      />
      <ContentLayoutRoute
        path={`${url}/cadastro-treino`}
        icon="file"
        titulo="Cadastro de Treino"
        component={Treinos}
      />
      <Route component={NotFound} />
    </Switch>
  );
};

export default PrivateRoutes;
