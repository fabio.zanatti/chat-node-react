import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import asyncComponent from "../Utils/AsyncFunc";

const AuthenticatedRoute = ({ component: Component, isLogged, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isLogged ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

const PublicRoutes = () => (
  <Router>
    <div style={{ height: "100%" }}>
      <Switch>
        <Route
          exact
          path="/"
          component={asyncComponent(() =>
            import("../Pages/Login")
          )}
        />
        {/* <Route
          exact
          path="/teste"
          component={asyncComponent(() =>
            import("../Pages/Teste")
          )}
        /> */}
        <AuthenticatedRoute
          path="/sys-monster-tech"
          isLogged
          component={asyncComponent(() =>
            import(
              "../Containers/PageLayout"
            )
          )}
        />
        <Route
          path="*"
          component={asyncComponent(() =>
            import("./Erro/404")
          )}
        />
      </Switch>
    </div>
  </Router>
);

export default PublicRoutes;
