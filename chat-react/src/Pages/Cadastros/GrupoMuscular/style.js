import styled from "styled-components";

const GrupoMuscularWrapper = styled.div`
  .card-content {
    display: flex;
    align-content: center;
    justify-content: center;
    .titulo-card {
      font-size: 16px;
      font-weight: 700;
    }
  }

  .card-adicionar {
    height: 100%;
    .ant-card-body{
        height: 100%;
    /* align-self: center; */
    display: flex;
    align-items: center;
    justify-content: center;
    }
  }
`;

export default GrupoMuscularWrapper;
