import React from "react";
import { Card, Col, Divider, Icon, Row } from "antd";
import { DadosContext } from "../../../Containers/PageLayout/DadosContext";
import GrupoMuscularWrapper from "./style";

class GrupoMuscular extends React.Component {
  state = {
    actionContext: this.context,
  };

  addGrupoMuscular() {
      console.log("add grupo")
    this.state.actionContext.testeAdd([
      ...this.state.actionContext.agrupamento,
      { nome: "Perna" },
    ]);

    console.log("array", [
        ...this.state.actionContext.agrupamento,
        { nome: "Perna" },
      ])
  }

  render() {
    console.log("actionContext", this.state.actionContext);
    return (
      <GrupoMuscularWrapper>
        <Row type="flex" justify="start" gutter={8}>
          <Col lg={6}>
            <Card className="card-adicionar" footer={false}>
              <div className="card-content">
                <span className="titulo-card" onClick={() => this.addGrupoMuscular()}>
                  <Icon type="plus" /> Adicionar
                </span>
              </div>
            </Card>
          </Col>
          {this.state.actionContext &&
            this.state.actionContext.agrupamento.map((item) => (
              <Col lg={6}>
                <Card
                  className="card-heigth"
                  actions={[
                    <Icon type="edit" key="edit" />,
                    <Icon type="delete" key="delete" />,
                  ]}
                >
                  <div className="card-content">
                    <span className="titulo-card">{item.nome}</span>
                  </div>
                </Card>
              </Col>
            ))}
        </Row>
      </GrupoMuscularWrapper>
    );
  }
}
GrupoMuscular.contextType = DadosContext;

export default GrupoMuscular;
