import React, { Suspense, lazy } from "react";
import Loading from "../../Components/Loading";

const Login = lazy(() => import("../../Containers/Login"))

const LoginPages = () => {
  return (
    <Suspense fallback={<Loading loading />}>
      <Login />
    </Suspense>
  );
};

export default LoginPages;
