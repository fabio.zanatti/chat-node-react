import styled from "styled-components";

const LayoutLogin = styled.div`
  height: 100%;
  display: flex;
  background-image: url("imagem3.jpg");
  background-repeat: no-repeat, repeat;
  background-size: auto;
  .login {
    width: 22%;
    margin: auto;

    .header-login {
      background-color: ${(props) => props.theme.primary[0]};
      position: relative;
      width: 90%;
      color: #fff;
      font-weight: 700;
      border-radius: 5px;
      padding: 30px;
      z-index: 9999;
      display: flex;
      justify-content: center;
      font-size: 20px;
      margin: auto;
    }

    .login-content {
      background-color: #ffffff;
      position: relative;
      top: -40px;
      padding: 60px 20px 20px 20px;
      border-radius: 5px;

      .style-icon-login {
        color: ${props => props.theme.placehoderColor[0]};
      }
    }
  }
`;

const BoxLogin = styled.div`
  width: 30%;
  background-color: #fff;
  border-radius: 3px;
`;

export { LayoutLogin, BoxLogin };
