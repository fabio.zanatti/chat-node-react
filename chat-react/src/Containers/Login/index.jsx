import React, { Suspense } from "react";
import { Form, Input, Icon } from "antd";
import { LayoutLogin } from "./style";
import ButtonLogin from "../../Components/ButtonLogin";
import Loading from "../../Components/Loading";

const Login = ({ route, form, ...props }) => {
  return (
    <Suspense fallback={<Loading />}>
    <LayoutLogin>
      <div className="login">
        <div className="header-login">Login</div>
        <div className="login-content">
          <div className="login-form">
            <Form>
              <Form.Item label="" required={false} colon={false}>
                {form.getFieldDecorator("cpf", {
                  rules: [{ required: true, message: "Obrigatório" }],
                })(
                  <Input
                    prefix={
                      <Icon type="user" className="style-icon-login" />
                    }
                    placeholder="CPF"
                  />
                )}
              </Form.Item>
              <Form.Item required={false} colon={false}>
                {form.getFieldDecorator("senha", {
                  rules: [{ required: true, message: "Obrigatório" }],
                })(
                  <Input
                    prefix={
                      <Icon type="lock" className="style-icon-login" />
                    }
                    type="password"
                    placeholder="Senha"
                  />
                )}
              </Form.Item>
            </Form>
          </div>
          <div className="footer-login">
            <ButtonLogin to="/sys-monster-tech" label="Entrar" />
          </div>
        </div>
      </div>
    </LayoutLogin>
    </Suspense>
  );
};

export default Form.create()(Login);
