import React from 'react';

const value = {
  agrupamentoMuscular: [{
      nome: "Peito"
  }],
  execicios: [],
};

export const DadosContext = React.createContext(value);
