import styled from "styled-components";

const PageLayoutWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  .menu-dash {
    height: 100%;
    background-color: blue;
  }

  .row-dash {
    display: flex;
    width: 100%;
  }

  .col-dash {
    display: block;
    width: 100%;

    .header-dash {
      width: 100%;
      height: 50px;
      border-bottom: solid 2px ${props => props.theme.gray[0]};
      background-color: #FFF
    }

    .content-dash {
      width: 100%;
      height: 100%;
      background-color: ${ props => props.theme.gray[0]};
    }
  }
`;

export default PageLayoutWrapper;
