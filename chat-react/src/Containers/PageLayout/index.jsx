import React, { Component, useEffect, useState } from "react";
import MenuDash from "../Menu";
import PrivateRoutes from "../../Routes/Private";
import PageLayoutWrapper from "./style";
import { BrowserRouter as Router } from "react-router-dom";
import Header from '../Header';
import { DadosContext } from "./DadosContext";

const value = {
  agrupamentoMuscular: [{
      nome: "Peito"
  }],
  execicios: [],
};

function PageLayout({ match }) {
  const [agrupamento, agrupamentoAdd] = useState();

  useEffect(() => {
    agrupamentoAdd([...value.agrupamentoMuscular])
  });
  
  function testeAdd(e) {
    console.log("e", e);
    agrupamentoAdd([...e]);
  }

  return (
    <PageLayoutWrapper>
      <DadosContext.Provider value={{
        agrupamento: [...agrupamento],
        agrupamentoAdd,
        testeAdd
      }}>
      <Router>
        <div className="row-dash">
          <MenuDash />
          <div className="col-dash">
            <div className="header-dash">
              <Header />
            </div>
            <div className="content-dash">
              <PrivateRoutes url={match.url} />
            </div>
          </div>
        </div>
      </Router>
      </DadosContext.Provider>
    </PageLayoutWrapper>
  );
}

export default PageLayout;
