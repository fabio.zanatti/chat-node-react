import React, { useState } from "react";
import { Button, Col, Icon, Row } from "antd";
import HeaderWrapper from './style';

function Header({ collapsed, toggleCollapsed }) {
  console.log('toggleCollapsed', toggleCollapsed);
  console.log('collapsed', collapsed);
  return (
    <React.Fragment>
      <HeaderWrapper>
        <Row className="header" type='flex'>
          <Col lg={2}>
            <Icon className="icon_menu_collapse" type={collapsed ? "menu-unfold" : "menu-fold"} onClick={() => toggleCollapsed(!collapsed)} />
          </Col>
          <Col lg={16} />
          <Col lg={1}><Icon width={20} height={20} type="notification" /></Col>
          <Col lg={5}>
            <div className="usuario">
                <Icon type="user" />
                <span className='nome-usuario'>Nome Usuário</span>
            </div>
          </Col>
        </Row>
      </HeaderWrapper>
    </React.Fragment>
  );
}

export default Header;
