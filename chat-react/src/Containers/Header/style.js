import styled from 'styled-components'

const HeaderWrapper = styled.div`
    .icon_menu_collapse { 
        font-size: 20px;
        margin-left: 5px;
    } 
    .header {
        padding: 12px 12px 12px 12px;
    }
    .usuario {
        display: flex;
        justify-content: start;
        align-items: center;
        align-self: center;
    }
    .nome-usuario {
        margin-left: 10px
    }
`;

export default HeaderWrapper