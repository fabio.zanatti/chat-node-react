import React from "react";
import { Menu } from "antd";
import menuItens from "./MenuItens";
import MenuWrapper from "./Style";
import { Link } from "react-router-dom";

const { SubMenu } = Menu;

function MenuDash({ collapsed }) {
  return (
    <MenuWrapper>
      <Menu
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
        theme="dark"
        inlineCollapsed={collapsed}
        className="menu"
      >
        {menuItens.map((item) => {
          if (item.subMenu) {
            return (
              <SubMenu key={item.key} title={<span>{item.nome}</span>}>
                {item.subMenu.map((itemSub) => (
                  <Menu.Item key={itemSub.key}>
                    <Link key={itemSub.link} to={itemSub.link}>
                      {itemSub.nomeItem}
                    </Link>
                  </Menu.Item>
                ))}
              </SubMenu>
            );
          } else {
            return (
              <Menu.Item key={item.key}>
                <Link key={item.link} to={item.link}>
                  <React.Fragment>
                    {item.icone && item.icone}
                    <span>{item.nome}</span>
                  </React.Fragment>
                </Link>
              </Menu.Item>
            );
          }
        })}
      </Menu>
    </MenuWrapper>
  );
}
export default MenuDash;
