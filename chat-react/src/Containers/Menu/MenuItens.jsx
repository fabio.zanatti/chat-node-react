import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRunning } from "@fortawesome/free-solid-svg-icons";
import { Icon } from "antd";

const menuItens = [
  {
    key: 1,
    nome: "Acompanhamento",
    icone: <Icon type="pie-chart" />,
    link: "/sys-monster-tech",
  },
  {
    key: 2,
    nome: "Treinos",
    icone: <FontAwesomeIcon icon={faRunning} />,
    subMenu: [
      {
        key: 21,
        nomeItem: "Cadastro de Grupo Muscular",
        link: "/sys-monster-tech/cadastro-grupo-muscular",
      },
      {
        key: 22,
        nomeItem: "Cadastro de Exercício",
        link: "/sys-monster-tech/cadastro-exercicios",
      },
      {
        key: 23,
        nomeItem: "Cadastro de Ficha de Treino",
        link: "/sys-monster-tech/cadastro-treino",
      },
    ],
  },
];

export default menuItens;
