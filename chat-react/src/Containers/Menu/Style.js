import styled from "styled-components";

const MenuWrapper = styled.div`
  .menu {
    height: 100%;
  }
  a {
   color: #fff;
  }
  a:hover {
    color: #fff;
  }

  .ant-menu.ant-menu-dark .ant-menu-item-selected,
  .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected {
    background-color: ${(props) => props.theme.primary[0]};
  }
`;

export default MenuWrapper;
