import styled from "styled-components";

const ContentLayoutWrapper = styled.div`
  .page-title {
    display: flex;
    justify-content: start;
    padding: 20px;
    border-top: solid 2px #e6e9ead9;
    border-radius: 3px;
    background-color: #fff;
    width: 100%;
  }
  .titulo {
    font-weight: 600;
    font-size: 20px;
    font-color: #000;
  }
  .page-row {
    padding: 25px;
  }
`;

export default ContentLayoutWrapper;
