import React from 'react';
import { Row, Icon, Card } from 'antd';
import ContentLayoutWrapper from './style';

const ContentLayout = ({
  children,
  titulo,
  icon,
}) => {
  return (
    <ContentLayoutWrapper>
      {titulo && (
        <Row type="flex">
          <span className="page-title">
            <span className="titulo">{titulo}</span>
          </span>
        </Row>
      )}
      <div className="page-row" id="page-row">
        {children}
      </div>
    </ContentLayoutWrapper>
  );
};

export default ContentLayout;
